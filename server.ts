var port = process.env.PORT || 8080;
var path = require('path');
var express = require('express');
var session = require('express-session');
var bodyParser = require('body-parser');
var http = require('http');
var bcrypt = require('bcrypt');
var uuid = require('uuid');

var app = express();

var version = "1.0.0";

// Set up view engine
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(express.static(path.join(__dirname, 'web')));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(session({ secret: 'sc' }));

app.get(['/', '/index'], function (req, res)
{
    res.render('index');
});

// Create node HTTP Server object
var server = http.createServer(app);

// Run HTTP Server
server.listen(port, function ()
{
    console.log('Server listening on port ' + port);
});

// fix something for v1.0.0
