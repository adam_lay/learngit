$(document).ready(function ()
{
    ContinuingDevelopment.Init();

    Test.DoThing();
});

var somethingThatBigFeatureNeeds = function ()
{
  console.log("Something that big feature needs");
};

var sommatElse = function ()
{
  console.log("Trying again");
};

class ContinuingDevelopment
{
    public static Init(): void
    {
        var foo = "bar";
    }

    public static Foo(bar: string): string
    {
      return bar + " another string";
    }

    public static Bar(): void
    {
      // Added whilst "something" was going on
    }
}

class Test
{
    public static DoThing(): void
    {
        var i = 1 + 3;
    }
}
